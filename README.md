## Underground Nexus - Athena0 - Kali Linux
> Kali Linux Bleeding Edge Respository, v0.9.0

CI/CD Pipeline Process

1. Source Repository
    - Azure DevOps (default)
    - GitLab (mirrored)
    - GitHub (mirrored)
2. Continuous Integration (CI)
    - Dagger.io (build locally)
        - I use a local registry for faster pull and push.
3. Continuous Deployment (CD)
    - Dagger.io (ship artifacts)
        - Development images are being sent to Azure Container Registry for local testing.
        - Official iamges are being sent to Dockerhub.
        - Others are sent to GitLab/GitHub Container Registry for development testing.
    - Azure DevOps (Pipelines)
    - Google Cloud Build (optional)
    - AWS CodeBuild (optional)
4. Container signing and verification
    - Sigstore Toolkit
        - Cosign is used to sign the container image.
        - SBOM and Attestation are attached to the development image and official image.
        - These official images are hosted at Dockerhub (phxvlabs).

Here are some of the steps I take to sign the container image with cosign. This is just an example.

```bash
docker pull phxvlabs/nexus-athena:amd64-latest

export VAULT_ADDR=http://localhost:8200
export VAULT_TOKEN=testtoken

cosign sign --key hashivault://nexus \
    -a commit=$(git rev-parse HEAD) \
    -a "repo=phxvlabs/nexus-athena" phxvlabs/nexus-athena:amd64-latest

cosign triangulate phxvlabs/nexus-athena:amd64-latest
index.docker.io/phxvlabs/nexus-athena:sha256-e934afc68874cd578e32a324ab15b8c986cd54c4e5e9cb089aeffae4b232a0e2.sig
```

### Information

This is single component of the entire Underground Nexus. Kali Linux Bleeding Repository is built from scratch with using `kalilinux/kali-bleeding-edge:amd64` as the base image. This newly built Docker image has `Wireshark`, `NMap`, `Metasploit Framework`, and rada`re2 for your use.

To get started with Underground Nexus, you will need to already have built the Underground Nexus. There is a script already in place to pull the necessary Docker image and run within the dockerized environment.

**Experimental**

Previously on a different GitHub repository, I had mirrored the project between GitHub and GitLab. The private key was generated via GitLab for signing. There is future plan to generate an official key as time goes by. There was a set of GitHub Actions in place with `trivy` scanning and building images to push to GitLab Container Registry and Dockerhub. However, it became too cumbersome, and I wanted a sole place where I can build agnostically.

Each of the docker images are built with Dagger and signed with cosign.