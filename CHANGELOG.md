**Latest Official Image**
> v0.8.0

image: phxvlabs/nexus-athena:amd64-latest
digest: sha256:e934afc68874cd578e32a324ab15b8c986cd54c4e5e9cb089aeffae4b232a0e2
sig: index.docker.io/phxvlabs/nexus-athena:sha256-e934afc68874cd578e32a324ab15b8c986cd54c4e5e9cb089aeffae4b232a0e2.sig

SBOM image:

image: index.docker.io/phxvlabs/nexus-athena:sha256-e934afc68874cd578e32a324ab15b8c986cd54c4e5e9cb089aeffae4b232a0e2.sbom
sig: phxvlabs/nexus-athena:sha256-e3d18cda3e93adcd151eda0a0b78e458d9ca77cae631ab1646489d7d0ba4a257.sig

Attestation image:

image: phxvlabs/nexus-athena:sha256-e934afc68874cd578e32a324ab15b8c986cd54c4e5e9cb089aeffae4b232a0e2.att

```bash
cosign verify --key cosign.pub phxvlabs/nexus-athena:amd64-latest | jq .

Verification for index.docker.io/phxvlabs/nexus-athena:amd64-latest --
The following checks were performed on each of these signatures:
  - The cosign claims were validated
  - The signatures were verified against the specified public key
[
  {
    "critical": {
      "identity": {
        "docker-reference": "index.docker.io/phxvlabs/nexus-athena"
      },
      "image": {
        "docker-manifest-digest": "sha256:e934afc68874cd578e32a324ab15b8c986cd54c4e5e9cb089aeffae4b232a0e2"
      },
      "type": "cosign container image signature"
    },
    "optional": {
      "commit": "6c10becb68350260757cfb2b86f8ba37750d015f",
      "repo": "phxvlabs/nexus-athena"
    }
  }
]
```