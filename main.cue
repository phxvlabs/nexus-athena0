package main

import (
	"dagger.io/dagger"
	"universe.dagger.io/bash"
	"universe.dagger.io/docker"

	"nexus-athena.io/kali"
)

dagger.#Plan & {
	client: network: "unix:///var/run/docker.sock": connect: dagger.#Socket
	client: env: {
		AZURE_CONTAINER_REGISTRY: string
		AZURE_CONTAINER_USER:     string
		AZURE_CONTAINER_PASS:     dagger.#Secret
		REGISTRY_USER:            string
		REGISTRY_PASS:            dagger.#Secret
		DOCKERIO_USER:            string
		DOCKERIO_PASS:            dagger.#Secret
	}
	actions: build: {
		kaliImage: {
			build: kali.#Build & {
				version: "amd64"
				packages: {
					"build-essential": {}
					gcc: {}
					"g++": {}
					git: {}
					"metasploit-framework": {}
					nmap: {}
					unzip: {}
					vim: {}
				}
			}
			push: docker.#Push & {
				auth: {
					username: "docker"
					secret:   client.env.REGISTRY_PASS
				}
				image: build.output
				dest:  "localhost:5000/nexus-athena-base:latest"
			}
		}
	}
	actions: ship: {
		latest:   _
		"v1.0.0": _

		[tag=string]: {
			build: docker.#Build & {
				steps: [
					docker.#Pull & {
						source:      "localhost:5000/nexus-athena-base:latest"
						resolveMode: "preferLocal"
						auth: {
							username: "docker"
							secret:   client.env.REGISTRY_PASS
						}
					},
					bash.#Run & {
						script: contents: """
							export DEBIAN_FRONTEND=noninteractive

							apt-get install wireshark -y -qq

							export TERRAFORM_VERSION=1.2.3

							wget -O terraform-amd64.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip

							unzip terraform-amd64.zip

							mv terraform usr/local/bin

							touch ~/.bashrc

							terraform -install-autocomplete

							rm terraform-amd64.zip
						"""
					},
					bash.#Run & {
						script: contents: """
							git clone https://github.com/radareorg/radare2

							sh radare2/sys/install.sh

							rm -rf radare2
						"""
					},
				]
			}
			push: docker.#Push & {
				image: build.output
				auth: {
					username: client.env.AZURE_CONTAINER_USER
					secret:   client.env.AZURE_CONTAINER_PASS
				}
				dest: "\(client.env.AZURE_CONTAINER_REGISTRY)/athena-kali-linux-test:\(tag)-amd64"
			}
		}
	}
	actions: shift: {
		build: docker.#Build & {
			steps: [
				docker.#Pull & {
					source: "\(client.env.AZURE_CONTAINER_REGISTRY)/athena-kali-linux-test:latest-amd64"
					auth: {
						username: client.env.AZURE_CONTAINER_USER
						secret:   client.env.AZURE_CONTAINER_PASS
					}
				},
			]
		}
		push: docker.#Push & {
			image: build.output
			auth: {
				username: client.env.DOCKERIO_USER
				secret:   client.env.DOCKERIO_PASS
			}
			dest: "\(client.env.DOCKERIO_USER)/nexus-athena:amd64-latest"
		}
	}
}
