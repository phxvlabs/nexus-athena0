package kali

import (
	"universe.dagger.io/docker"
)

#Build: {
	version: "amd64" | "arm64" | *"latest" | string
	packages: [pkgName=string]: version: string | *""
	_pkgList: [
		for pkgName, pkg in packages {
			"\(pkgName)\(pkg.version)"
		},
	]

	docker.#Build & {
		steps: [
			docker.#Pull & {
				source: "index.docker.io/kalilinux/kali-bleeding-edge:\(version)"
			},
			docker.#Run & {
				env: {
					DEBIAN_FRONTEND: "noninteractive"
				}
				command: {
					name: "apt-get"
					args: ["update"]
					flags: {
						"-y": true
					}
				}
			},
			docker.#Run & {
				command: {
					name: "apt-get"
					args: ["install", ...] + _pkgList
					flags: {
						"-y":                      true
						"--no-install-recommends": true
					}
				}
			},
			docker.#Run & {
				command: {
					name: "apt-get"
					args: ["clean"]
				}
			}
		]
	}
}
